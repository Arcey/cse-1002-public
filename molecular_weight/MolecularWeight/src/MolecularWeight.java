/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Molecular Weight
*/

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public final class MolecularWeight {
   static final String CSV_DELIMITER = ",";
   static final String EQ_DELIMITER = " ";

   public record Entry (String symbol, double weight) {}

   public static void main (final String[] args) throws IOException {
      final ArrayList<Entry> database = populateDatabse(Paths.get(args[0]));
      final Scanner stdin = new Scanner(System.in, "US-ASCII");

      while (stdin.hasNext()) {
         final String equation = stdin.nextLine();
         System.out.printf("Molecular weight of %s= %s%n", equation,
               molecularWeight(equation, database));
      }

      stdin.close();
   }

   // Takes a String describing the equation to calculate the weight
   private static String molecularWeight (final String eq, final ArrayList<Entry> db) {
      final Scanner eqScanner = new Scanner(eq);

      double molecularWeight = 0;
      while (eqScanner.hasNext()) {
         try {
            final double symbolWeight = findSymbol(eqScanner.next(), db).weight();

            int quantity = 1; // Assumed 1 unless otherwise specified
            if (eqScanner.hasNextInt()) {
               quantity = eqScanner.nextInt();
            }
            molecularWeight += symbolWeight * quantity;
         } 
         catch (final NullPointerException npe) {
            // Null means a symbol is not in the database, equation cannot be solved
            return "??";
         }
      }

      eqScanner.close();
      return String.format("%.2f", molecularWeight);
   }

   // Find and return an Entry in the db based on given symbol, returns null if not found
   private static Entry findSymbol (final String symbol, final ArrayList<Entry> db) {
      for (final Entry entry : db) {
         if (entry.symbol.equals(symbol)) {
            return entry;
         }
      }
      return null;
   }

   // IOException required for filePath
   private static ArrayList<Entry> populateDatabse (final Path path) throws IOException {
      final ArrayList<Entry> database = new ArrayList<Entry>();
      final Scanner filein = new Scanner(path, "US-ASCII").useDelimiter(CSV_DELIMITER);

      // Skip column headers in csv
      filein.nextLine();

      while (filein.hasNext()) {
         filein.next(); // Ignore Element Name
         filein.next(); // Ignore Entry Number
         // Store atomic symbol and weight in a new Entry
         database.add(new Entry(filein.next(), filein.nextDouble()));
         filein.nextLine(); // Ignore everything else
      }

      filein.close();
      return database;
   }
}
