import java.awt.event.KeyEvent;
import java.util.Random;

public class PercolationVisualizer {

    private static final Random RNG = new Random(Long.getLong("seed",
            System.nanoTime()));

    public static void main (String[] args) {
        int n = Integer.parseInt(args[0]);
        double p = Double.parseDouble(args[1]);
        int trials = Integer.parseInt(args[2]);

        StdDraw.clear(StdDraw.LIGHT_GRAY);

        // repeatedly created n-by-n matrices and display them using standard draw
        StdDraw.enableDoubleBuffering();
        for (int t = 0; t < trials; t++) {
            if (StdDraw.isMousePressed()) {
                StdDraw.pause(10000);
            }
            boolean[][] open = randomSiteMap(n, p);
            StdDraw.clear(StdDraw.LIGHT_GRAY);
            StdDraw.setPenColor(StdDraw.BLACK);
            Percolation.show(open, false);
            StdDraw.setPenColor(StdDraw.BOOK_LIGHT_BLUE);
            boolean[][] full = Percolation.flow(open);
            Percolation.show(full, true);
            StdDraw.show();
            StdDraw.pause(1000);
        }
    }

    private static boolean[][] randomSiteMap (final int n, final double p) {
        final boolean[][] sites = new boolean[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                // sites[i][j] = RNG.nextBoolean();
                sites[i][j] = RNG.nextBoolean();
            }
        }

        return sites;
    }
}
