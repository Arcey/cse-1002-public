/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Percolation
*/

import java.util.Random;

public final class Estimate {
   private static final Random RNG = new Random(Long.getLong("seed",
         System.nanoTime()));

   public static void main (final String[] args) {
      final int n = Integer.parseInt(args[0]);
      final double p = Double.parseDouble(args[1]);
      final int trials = Integer.parseInt(args[2]);
      System.out.printf("%.2f%n", evaluate(n, p, trials));
   }

   // do specified number of trials and return fraction that percolate
   private static double evaluate (final int n, final double p, final int trials) {
      double count = 0;
      for (int t = 0; t < trials; t++) {
         final boolean[][] isOpen = randomSiteMap(n, p);
         if ((!VerticalPercolation.percolates(isOpen) && Percolation2.percolates(isOpen)
               && Percolation.percolates(isOpen))) {
            count++;
         }
      }
      return count / trials;
   }

   // generate an n-by-n 2d array of random t/f values
   private static boolean[][] randomSiteMap (final int n, final double p) {
      final boolean[][] sites = new boolean[n][n];

      for (int i = 0; i < n; i++) {
         for (int j = 0; j < n; j++) {
            sites[i][j] = RNG.nextBoolean();
         }
      }

      return sites;
   }
}
