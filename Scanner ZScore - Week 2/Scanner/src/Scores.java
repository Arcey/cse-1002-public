/*
* Author:    Vincent Quintero
* Course:    CSE 1002, Spring 2022
* Project:   Proj 05, Z-Scores
*/

import java.util.Scanner;

public class Scores {

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner (System.in, "US-ASCII");
      final int numStudents = scanner.nextInt ();
      final String[] personFirst = new String[numStudents];
      final String[] personLast = new String[numStudents];
      final double[] personScore = new double[numStudents];

      // The input order is expected to be First, Last, Score
      for (int i = 0; i < numStudents; i++) {
         personFirst[i] = scanner.next ();
         personLast[i] = scanner.next();
         personScore[i] = scanner.nextDouble ();
      }

      final double[] zScores = zScores (personScore);

      // Display the table headers before showing data
      System.out.printf ("%-31s %5s %5s %5s%n", "Name", "Score", "Z", "Grade");

      for (int i = 0; i < numStudents; i++) {
         printRow (personFirst[i], personLast[i], personScore[i], zScores[i],
               grade (zScores[i]));
      }

      // Display class average
      System.out.printf ("Average %.2f%n", sum (personScore) / personScore.length);

      // Close scanner to prevent resource leaks
      scanner.close ();
   }

   // Display a student's name, score, and grade to the screen
   private static void printRow (final String firstName, final String lastName,
         final double score, final double zScore, final char grade) {
      System.out.printf ("%15s %-15s %5.0f %5.2f %5c%n", firstName, lastName, score,
            zScore, grade);
   }

   // Calculate a Z-score based on the overall class score
   // Z = (score - avgScore) / stdDev
   private static double[] zScores (final double[] classScores) {
      final double avgGrade = sum (classScores) / classScores.length;
      final double stdDev = stdDev (classScores);
      final double[] zScores = new double[classScores.length];

      for (int i = 0; i < classScores.length; i++) {
         zScores[i] = (classScores[i] - avgGrade) / stdDev;
      }

      return zScores;
   }

   // Calculate the class' Standard Population Deviation from their scores
   // stdDev = sqrt( SUM(score[i] - avg)^2 / classScores.length )
   private static double stdDev (final double[] classScores) {
      final double classTotalScore = sum (classScores);
      final double classAvg = classTotalScore / classScores.length;

      // Calculate the function within the sigma in the formula's numerator
      final double[] fracTop = new double[classScores.length];

      for (int i = 0; i < classScores.length; i++) {
         fracTop[i] = Math.pow (classScores[i] - classAvg, 2);
      }

      final double numerator = sum (fracTop);

      return Math.sqrt (numerator / classScores.length);
   }

   // Return the sum of the contents of an array of numbers
   private static double sum (final double[] numbers) {
      double total = 0;
      for (final double n : numbers) {
         total += n;
      }
      return total;
   }

   // Determine a student's grade based on their Z-score
   // A >= 1, 1 > B >= 0, 0 > C >= -1, -1 > D >= -2, -2 > F
   private static char grade (final double zScore) {
      if (zScore >= 1) {
         return 'A';
      } else if (zScore >= 0) {
         return 'B';

      } else if (zScore >= -1) {
         return 'C';

      } else if (zScore >= -2) {
         return 'D';
      } else {
         return 'F';
      }
   }
}
