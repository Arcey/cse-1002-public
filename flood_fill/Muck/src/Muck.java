
/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Sand searching
*/
import java.util.Scanner;

public final class Muck {
   static final char MUCK = 'M';
   static final char SAND = '.';
   static final char MARK = 'x';
   // The minimum number of neighbors necessary to form a region
   static final int ADJ_MIN = 1;

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner(System.in, "US-ASCII");
      final int rows = scanner.nextInt(); // N rows
      final int cols = scanner.nextInt(); // M cols
      scanner.nextLine(); // Clear hanging newline after reading ints

      final char[][] muckMap = mapMuck(scanner, rows, cols);

      int regions = 0;

      for (int i = 0; i < rows; i++) {
         for (int j = 0; j < cols; j++) {
            if (floodRegion(muckMap, j, i) >= ADJ_MIN) {
               ++regions;
            }
         }
      }

      System.out.printf("%d%n", regions);

      scanner.close();
   }

   // Reads in user inputted series of characters as a map of sand/muck
   private static char[][] mapMuck (final Scanner scanner, final int rows, 
         final int cols) {
      final char[][] muckMap = new char[rows][cols];

      // Each line is its own row, so cols are unecessary to gather
      for (int i = 0; i < rows; i++) {
         muckMap[i] = scanner.nextLine().toCharArray();
      }

      return muckMap;
   }

   // Recursively floods a region, returning total number of neighbors flooded
   private static int floodRegion (final char[][] area, final int currX,
         final int currY) {
      final int yDim = area.length;
      final int xDim = area[0].length;

      // Ensure position point is not out of bounds
      if (currX >= xDim || currY >= yDim || currX < 0 || currY < 0) {
         return 0;
      }

      int neighbors = 0;

      if (area[currY][currX] == SAND) {
         area[currY][currX] = MARK;
         neighbors++;

         neighbors += floodRegion(area, currX, currY - 1); // Try Flood above current
         neighbors += floodRegion(area, currX - 1, currY - 1); // Try Flood topleft
         neighbors += floodRegion(area, currX + 1, currY - 1); // Try Flood topright
         neighbors += floodRegion(area, currX, currY + 1); // Try Flood below current
         neighbors += floodRegion(area, currX - 1, currY + 1); // Try Flood bottom left
         neighbors += floodRegion(area, currX + 1, currY + 1); // Try Flood bottom right
         neighbors += floodRegion(area, currX - 1, currY); // Try Flood left current
         neighbors += floodRegion(area, currX + 1, currY); // Try Flood right current
      }

      return neighbors;
   }
}
