/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Buffon's Needle
*/

import java.util.Random;

public final class Needle {

   private static final Random RNG = new Random(Long.getLong("seed", System.nanoTime()));
   private static final int RADIUS = 1;

   public static void main (final String[] args) {
      final int darts = Integer.parseInt(args[0]);
      double inCircle = 0;

      for (int i = 0; i < darts; i++) {
         final double x = RNG.nextDouble();
         final double y = RNG.nextDouble();

         // Check if point lies within the circle using
         // x^2 + y^2 <= r^2
         if (Math.pow(x, 2) + Math.pow(y, 2) <= Math.pow(RADIUS, 2)) {
            inCircle++;
         }
      }

      // inCircle / darts gives ratio of ~ pi/4. Multiply to get true pi
      System.out.printf("%.6f%n", (inCircle / darts) * 4);
   }
}
