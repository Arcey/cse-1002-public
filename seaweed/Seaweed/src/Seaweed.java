/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Seaweed Disease Research
*/

import java.util.Arrays;
import java.util.Scanner;

public final class Seaweed {
   private Seaweed () {};

   // Distance travelled in hours, seaweed consumed on said travel (lbs)
   // ! Must be stored as doubles because casting will not pass stylecheck
   private record Manatee (double travelTime, double consumption)
         implements Comparable<Manatee> {
      @java.lang.Override
      public int compareTo (final Manatee other) {
         final double m1 = this.consumption / this.travelTime;
         final double m2 = other.consumption / other.travelTime;
         if (m1 > m2) {
            return -1;
         } else if (m1 < m2) {
            return 1;
         }
         return 0;
      }
   };

   public static void main (final String[] args) {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");
      final int numManatees = stdin.nextInt();
      stdin.nextLine(); // Clear hanging newline after reading int
      final Manatee[] manatees = new Manatee[numManatees];

      for (int i = 0; i < numManatees; i++) {
         manatees[i] = new Manatee(stdin.nextInt(), stdin.nextInt());
      }

      Arrays.sort(manatees);

      int totalConsumption = 0;
      // -1 because checking the last element is unnecessary (Last manatee doesn't eat)
      for (int i = 0; i < manatees.length - 1; i++) {
         // Double travelTime to account for the trip back, it is a round trip after all
         totalConsumption += calcConsumption(manatees[i].travelTime * 2, i + 1,
               manatees.length, manatees);
      }

      System.out.printf("%d%n", totalConsumption);

      stdin.close();
   }

   // Calc & sum every manatee's consumption from start to stop in the given list
   private static int calcConsumption (final double travelTime, final int start,
         final int stop, final Manatee[] manatees) {
      int totalConsumption = 0;
      for (int i = start; i < stop; i++) {
         totalConsumption += manatees[i].consumption * travelTime;
      }
      return totalConsumption;
   }
}
