/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Mortgage
*/

import java.util.Scanner;
import java.math.BigDecimal;
import java.math.RoundingMode;

public final class Mortgage {

   static final String BALANCE_KEYWORD = "balance";
   static final int PRECISION = 2;

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner(System.in, "US-ASCII");
      BigDecimal balance = new BigDecimal(scanner.next());
      final BigDecimal rate = new BigDecimal(scanner.next());
      balance = balance.setScale(PRECISION, RoundingMode.HALF_UP);

      while (scanner.hasNext()) {
         final String request = scanner.next();

         // User is requesting to view their balance
         if (request.equalsIgnoreCase(BALANCE_KEYWORD)) {
            System.out.printf("%s%n", showBalance(balance));

         } else {
            final BigDecimal payment = new BigDecimal(request);
            balance = makePayment(balance, rate, payment);
         }
      }

      scanner.close();
   }

   // Returns a NEW BigDecimal calculated from the following: Apply a given interest
   // rate to the current balance, then subtract the amount paid from calculated balance
   private static BigDecimal makePayment (final BigDecimal balOwed, final BigDecimal rate,
         final BigDecimal payment) {
      final BigDecimal balLeft = (balOwed.add(balOwed.multiply(rate))).subtract(payment);
      return balLeft.setScale(PRECISION, RoundingMode.HALF_UP);

   }

   // Return a non-terminating string representation of the current balance
   private static String showBalance (final BigDecimal balance) {
      final int comparedBal = balance.compareTo(BigDecimal.ZERO);
      final String message = "balance: ";

      if (comparedBal > 0) {
         // Balance is nonzero & nonnegative
         return message + String.format("%s left", balance);
      } else if (comparedBal < 0) {
         // Balance is nonzero and negative
         return message + String.format("%s over", balance.abs());
      }

      // Balance is zero
      return message + balance;
   }
}
