/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Guitar
*/

import java.util.ArrayList;
import java.util.Scanner;

public final class GuitarHero {
   public static void main (final String[] args) {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");
      final ArrayList<Double> audio = new ArrayList<Double>(); 

      while (stdin.hasNext()) {
         final int noteIndex = stdin.nextInt();
         final double duration = stdin.nextDouble();
         final double frequency = 440 * Math.pow(2, (noteIndex / 12));

         final GuitarString gs = new GuitarString(frequency);
         gs.pluck();

         for (int i = 0; i <= GuitarString.SAMPLE_RATE * duration; i++) {
            gs.tic();
            audio.add(gs.sample());
         }
      }
      
      MyAudio.play(audio.stream().mapToDouble(Double::doubleValue).toArray());
      MyAudio.close();
      stdin.close();
   }
}
