/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Guitar
*/

import java.util.ArrayDeque;
import java.util.Random;

public final class GuitarString {

   static final Random RNG = new Random(Long.getLong("seed", System.nanoTime()));
   static final int SAMPLE_RATE = 44_100;
   static final double DECAY_FACTOR = 0.996;
   static final double MAX_DISP = 0.5;
   static final double MIN_DISP = -0.5;

   final double frequency;
   final ArrayDeque<Double> buffer;
   final int bufferSize;

   public GuitarString (final double frequency) {
      this.frequency = frequency;
      this.buffer = new ArrayDeque<Double>();
      this.bufferSize = Math.toIntExact(Math.round(SAMPLE_RATE / frequency));
   }

   // Add [bufferSize] number of elements to [string] between +-0.5
   public void pluck () {
      for (int i = 0; i < this.bufferSize; i++) {
         // Add 0.01 to account for nextDouble being exclusive and never getting to 1.00
         this.buffer.add((RNG.nextDouble() * (MAX_DISP - MIN_DISP)) + MIN_DISP);
      }
   }

   // Pop first element in string and append avg of (popped element + new first) * decay
   // to end of the queue
   public void tic () {
      // Average the two front values, then multiply by DECAY_FACTOR = newX
      final double newX = ((this.buffer.pop() + this.buffer.getFirst()) 
            * (MAX_DISP - MIN_DISP)/ 2) * DECAY_FACTOR;
      // Push this newX to the end of the queue
      this.buffer.addLast(newX);
   }

   public double sample () {
      return this.buffer.getFirst();
   }
}
