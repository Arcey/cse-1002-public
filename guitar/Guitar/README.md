Guitar String
Simulate the plucking of a guitar string:
To simulate plucking we can use the Karplus-Strong string synthesis algorithm. When a guitar string is plucked, the string vibrates and creates sound. The length of the string determines its fundamental frequency of vibration. We model a guitar string by sampling its displacement (a real number between -1/2 and +1/2) at N equally spaced points (in time), where N equals the sampling rate (44,100) divided by the fundamental frequency (rounded to the nearest integer). guitar-samples

Plucking the string:
The excitation of the string can contain energy at any frequency. We simulate the excitation by filling the buffer with white noise: set each of the N sample displacements to a random real number between -1/2 and +1/2.
white-noises
The resulting vibrations:
After the string is plucked, the string vibrates. The pluck causes a displacement which spreads wave-like over time. The Karplus-Strong algorithm simulates this vibration by maintaining a ring buffer of the N samples: the algorithm repeatedly deletes the first sample from the buffer and adds to the end of the buffer the average of the first two samples, scaled by an energy decay factor of 0.996.
karplus-strong
Why it works?
The two primary components that make the Karplus-Strong algorithm work are the ring buffer feedback mechanism and the averaging operation.

The ring buffer feedback mechanism:
The ring buffer models the medium (a string tied down at both ends) in which the energy travels back and forth. The length of the ring buffer determines the fundamental frequency of the resulting sound. Sonically, the feedback mechanism reinforces only the fundamental frequency and its harmonics (frequencies at integer multiples of the fundamental). The energy decay factor (.996 in this case) models the slight dissipation in energy as the wave makes a roundtrip through the string.
The averaging operation:
The averaging operation serves as a gentle low pass filter (which removes higher frequencies while allowing lower frequencies to pass, hence the name). Because it is in the path of the feedback, this has the effect of gradually attenuating the higher harmonics while keeping the lower ones, which corresponds closely with how actually plucked strings sound.
Task
1. GuitarString: Write a class to model a vibrating guitar string of a given frequency. Implement a class named GuitarString. This class should make use of an ArrayDeque as a ring buffer. The class GuitarString should implement the following methods.

    GuitarString(double frequency)     // create a guitar string of the given frequency
                                       // and initialize the ArrayDeque
    void pluck()                       // set the buffer to white noise
    void tic()                         // advance the simulation one time step
    double sample()                    // return the current sample
Constructor: The constructor creates an ArrayDeque and stores the frequency to be used in pluck method. Recall that the buffer size will be the sampling rate(44,100) divided by the frequency (rounding the quotient up to the nearest integer).
Pluck: Add the N elements in the buffer with N random values between -0.5 and +0.5 ( N is sample rate(44,100) divided by frequency).
Tic: Apply the Karplus-Strong update: delete the sample at the front of the buffer and add to the end of the buffer the average of the first two samples, multiplied by the energy decay factor.
Sample: Return the value of the item at the front of the buffer.
2. GuitarHero: Write a client program to play notes from standard input using the MyAudio class (not StdAudio) from stdaudio.jar. Here is the documentation for StdAudio. Here is the sample program for StdAudio for your reference. The ith note corresponds to a frequency of 440*(2^(i /12)), so that note A is 440 Hz, note C is 523.25 Hz. Don't even think of including 13 individual GuitarString variables or a 13-way if statement! Instead, create a GuitarString each time using the equation for frequency. The notes are played from a standard input that contains (note index, duration in seconds ) pairs.
notes

Sample Input:

7 .25
6 .25
7 .25
6 .25
7 .25
2 .25
5 .25
3 .25
0 .50
-9 .25
-5 .25
0 .25
2 .50
-5 .25
-1 .25
2 .25
3 .50
7 .25
6 .25
7 .25
6 .25
7 .25
2 .25
5 .25
3 .25
0 .50
-9 .25
-5 .25
0 .25
2 .50
-5 .25
3 .25
2 .25
0 .50
Another Sample Input ("Song of Storms" from the video game "Ocarina of Time" thanks to Kim Day):
-7 0.25
-4 0.25
5 0.50
-7 0.25
-4 0.25
5 0.50
7 0.50
8 0.25
7 0.25
8 0.25
7 0.25
3 0.25
0 0.75
Hint:
Watch this video

Submit:
GuitarString.java, GuitarHero.java and your choice of input which follows the format of the given sample in a file, MyInput.txt
