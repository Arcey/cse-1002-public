/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Oscar Poll
*/

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public final class Movie {

   public record Vote (int film, int round1, int round2) {};

   public static void main (final String[] args) {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");
      final int filmCount = stdin.nextInt();
      final int filmsAdvancing = stdin.nextInt();
      final Vote[] nominees = new Vote[filmCount];

      for (int i = 0; i < filmCount; i++) {
         nominees[i] = new Vote(i + 1, stdin.nextInt(), stdin.nextInt());
      }

      Arrays.sort(nominees, Comparator.comparing(Vote::round1).reversed()
            .thenComparing((v1, v2) -> v2.film - v1.film));
      final Vote[] finalists = Arrays.copyOfRange(nominees, 0, filmsAdvancing);
      Arrays.sort(finalists, Comparator.comparing(Vote::round2).reversed()
            .thenComparing((v1, v2) -> v2.film - v1.film));

      System.out.printf("%d %d%n",
            finalists[0].film, finalists[finalists.length - 1].film);
      stdin.close();
   }
}
