/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: My ArrayList
*/

import java.util.Arrays;

public final class MyArrayList extends MyListInterface {
   private static final int INITIAL_CAPACITY = 10;
   private static final int RESIZE_FACTOR = 2; // Factor multiplied by when resizing
   private Integer[] array;
   private int size = 0; // Elements in the array

   public MyArrayList () {
      this(INITIAL_CAPACITY);
   }

   public MyArrayList (final int initialCapacity) {
      this.array = new Integer[initialCapacity];
   }

   private void ensureCapacity (final int minCapacity) {
      if (this.array.length < minCapacity) {
         this.array = Arrays.copyOf(this.array, minCapacity);
      }
   }

   @Override
   public int size () {
      return this.size;
   }

   @Override
   // Retain capacity while nulling out all elements
   public void clear () {
      for (int i = 0; i < this.size; i++) {
         this.array[i] = null;
      }
      this.size = 0;
   }

   @Override
   public boolean add (final Integer element) {
      if (this.size == this.array.length) {
         this.ensureCapacity(this.array.length * MyArrayList.RESIZE_FACTOR);
      }
      this.array[this.size] = element;
      this.size++;
      return true;
   }

   @Override
   public Integer get (final int index) throws IndexOutOfBoundsException {
      // Throw error on accessing null value?
      if (this.validIndex(index, this.array.length)) {
         return this.array[index];
      }
      throw new IndexOutOfBoundsException();
   }

   @Override
   public Integer set (final int index, final Integer element)
         throws IndexOutOfBoundsException {
      if (this.validIndex(index, this.array.length)) {
         final int removed = this.array[index];
         this.array[index] = element;
         return removed;
      }
      throw new IndexOutOfBoundsException();
   }

   @Override
   public Integer remove (final int index) throws IndexOutOfBoundsException {
      if (this.validIndex(index, this.array.length)) {
         final int removed = this.array[index];
         this.shift(index);
         return removed;
      }
      throw new IndexOutOfBoundsException();
   }

   @Override
   public boolean remove (final Integer element) {
      for (int i = 0; i < this.size; i++) {
         if (this.array[i] != null && this.array[i].equals(element)) {
            this.shift(i);
            return true;
         }
      }

      return false;
   }

   @Override
   public String toString () {
      if (this.size == 0) {
         return "[]";
      }

      final StringBuilder sb = new StringBuilder("[");
      // Go until all (this.size) elements are found
      int i = 0; // Track non-null elements found
      for (final Integer n : this.array) {
         if (n != null) {
            sb.append(n.toString());
            i += 1;
            if (i == this.size) {
               sb.append("]");
               return sb.toString();
            }
            sb.append(", ");
         }
      }
      // Unreachable, compiler requires it though, error would be ideal but beyond scope
      return sb.toString();
   }

   // Checks if index is valid (index > 0) and (index within array bounds) and (size > 0)
   // Range is exclusive
   private boolean validIndex (final int index, final int maxRange) {
      // Nonzero elements in array & nonegative index & index & size can't go OOB
      if (this.size > 0 && index >= 0 && index < maxRange && this.size <= maxRange) {
         return true;
      }
      return false;
   }

   // Shift array up to index (exclusive), value at index is lost
   private void shift (final int index) {
      /* Copy elements up to index of removed element, paste where removed element was
       * Start at this.size + 1 to skip element being removed
       * Paste in the array where removed element was (index currently holds null)
       * Take the size, minus 1 element removed, minus index for size of shift
       */ 
      this.size--;
      System.arraycopy(this.array, index + 1, this.array,
            index, this.size - index);
      this.array[this.size] = null; // Null out where last element was before shift
   }
}
