import java.util.ArrayList;
import java.util.Arrays;

public class Main {
   public static void main (String[] args) {
      ArrayList<Integer> l = new ArrayList<Integer>(12);
      MyArrayList ml = new MyArrayList(12);
      System.out.printf("Size Test%n");
      System.out.printf("Expect: %d%n", l.size());
      System.out.printf("Actual: %d%n", ml.size());

      for (int i = 0; i < 35; i++) {
         l.add(i);
         ml.add(i);
      }

      System.out.printf("Add Test%n");
      System.out.printf("Expect: %d - %s%n", l.size(), l.toString());
      System.out.printf("Actual: %d - %s%n", ml.size(), ml.toString());
      l.clear();
      ml.clear();

      l.add(5);
      l.add(6);
      ml.add(5);
      ml.add(6);
      System.out.printf("Clear & Add Test%n");
      System.out.printf("Expect: %s%n", l.toString());
      System.out.printf("Actual: %s%n", ml.toString());

      System.out.printf("Remove Test%n");
      ml.remove(0);
      ml.add(16);
      l.remove(0);
      l.add(16);
      System.out.printf("Expect: %s%n", l.toString());
      System.out.printf("Actual: %s%n", ml.toString());

      System.out.printf("Get Test%n");
      System.out.printf("Expect: %d%n", l.get(0));
      System.out.printf("Actual: %d%n", ml.get(0));

      System.out.printf("Set Test%n");
      ml.set(0, 9);
      l.set(0, 9);
      System.out.printf("Expect: %s%n", l.toString());
      System.out.printf("Actual: %s%n", ml.toString());

      System.out.printf("Clear Test%n");
      ml.clear();
      l.clear();
      System.out.printf("Expect: %s%n", l.toString());
      System.out.printf("Actual: %s%n", ml.toString());

      System.out.printf("Remove Existing Value Test%n");
      ml.add(5);
      l.add(5);
      System.out.printf("Expect: %s%n", l.remove(Integer.valueOf(5)));
      System.out.printf("Actual: %s%n", ml.remove(Integer.valueOf(5)));

      System.out.printf("Remove Nonexistent Value Test%n");
      System.out.printf("Expect: %s%n", l.remove(Integer.valueOf(64)));
      System.out.printf("Actual: %s%n", ml.remove(Integer.valueOf(64)));

      System.out.printf("Expect: %s%n", l.toString());
      System.out.printf("Actual: %s%n", ml.toString());

      System.out.printf("Remove NULL Test%n");
      try {
         System.out.printf("Expect: %s%n", l.remove(1));
      } catch (IndexOutOfBoundsException oob) {
         System.out.printf("Expect: OOB%n");
      }
      try {
         System.out.printf("Actual: %s%n", ml.remove(1));
      } catch (IndexOutOfBoundsException oob) {
         System.out.printf("Actual: OOB%n");
      }

      System.out.printf("Get NULL Test%n");
      try {
         System.out.printf("Expect: %s%n", l.get(1));
      } catch (IndexOutOfBoundsException oob) {
         System.out.printf("Expect: OOB%n");
      }
      try {
         System.out.printf("Actual: %s%n", ml.get(1));
      } catch (IndexOutOfBoundsException oob) {
         System.out.printf("Actual: OOB%n");
      }
   }
}
