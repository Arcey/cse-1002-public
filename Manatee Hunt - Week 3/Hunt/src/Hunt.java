/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Manatee Hunt
*/

import java.util.Scanner;

public final class Hunt {

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner(System.in, "US-ASCII");
      // Sub 1 since the very first hiding spot will have always been checked
      final int hidingSpots = scanner.nextInt() - 1;
      final int totalStudents = scanner.nextInt();
      final int minGroupSize = scanner.nextInt();
      final double numGroups = Math.floor(totalStudents / minGroupSize);

      System.out.printf("%.0f%n", Math.ceil(hidingSpots / numGroups));

      scanner.close();
   }
}
