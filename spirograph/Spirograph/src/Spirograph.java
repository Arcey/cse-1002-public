/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Spirograph
*/

public final class Spirograph {

   static final double ORIGIN_X = 512;
   static final double ORIGIN_Y = 512;
   static final double STEP_COUNT = 0.01;
   static final double MAX_STEPS = 100;

   public static void main (final String[] args) {
      final double largeR = Double.parseDouble(args[0]);
      final double smallR = Double.parseDouble(args[1]);
      final double penOffset = Double.parseDouble(args[2]);

      // Prepare canvas for spirograph
      StdDraw.setScale(0.0, 1024.0);
      StdDraw.clear(StdDraw.BLACK);
      StdDraw.setPenColor(StdDraw.WHITE);

      for (double i = 0; i <= MAX_STEPS; i += STEP_COUNT) {
         final double x = (largeR + smallR) * Math.cos(i)
               - (smallR + penOffset) * Math.cos(((largeR + smallR) / smallR) * i);
         final double y = (largeR + smallR) * Math.sin(i)
               - (smallR + penOffset) * Math.sin(((largeR + smallR) / smallR) * i);

         StdDraw.line(x + ORIGIN_X, y + ORIGIN_Y, x + ORIGIN_X, y + ORIGIN_Y);
      }
   }
}
