/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Crabby
*/

import java.util.Scanner;

public final class Crabby {

   static final int ALPHABET = 26;

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner(System.in, "US-ASCII");

      while (scanner.hasNext()) {
         final String text = scanner.next();
         findPalindrome(text);
      }

      scanner.close();
   }

   private static void findPalindrome (final String s) {
      final int[] charCount = new int[ALPHABET];

      for (int i = 0; i < s.length(); i++) {
         // Get 0 based offset from a given lowercase character
         final int cIndex = s.charAt(i) - 'a';
         charCount[cIndex] = (charCount[cIndex] + 1);
      }

      System.out.printf("%d%n", forcePalindrome(charCount));
   }

   // Remove any characters necessary to make this string a palindrome
   private static int forcePalindrome (final int[] charCount) {
      int lettersRemoved = 0;
      boolean firstOddFound = false;

      for (int i = 0; i < charCount.length; i++) {
         if (charCount[i] % 2 != 0) {
            // Not our first odd character, take one off
            if (firstOddFound) {
               charCount[i] = charCount[i] - 1;
               lettersRemoved++;
            } else {
               firstOddFound = true;
            }
         }
      }

      return lettersRemoved;
   }
}
