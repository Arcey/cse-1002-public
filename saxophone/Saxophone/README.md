Due: April 17 2022, Sunday before 8pm

Additional practice is always available at CodingBat Java, Project Euler, and Kattis.

The Task
Yamaha Saxophone YAS-62
When playing music on an Eb Alto Saxophone, the fingers press some keys more than others. For simplicity assume that the music is composed of only 
14
 different notes. They are: C D E F G A B in one octave and C D E F G A B in a higher octave. We use c,d,e,f,g,a,b,C,D,E,F,G,A,B to represent them. The fingers typically used for each note are:

c: finger 
2
-
4
, 
7
-
10

d: finger 
2
-
4
, 
7
-
9

e: finger 
2
-
4
, 
7
, 
8

f: finger 
2
-
4
, 
7

g: finger 
2
-
4

a: finger 
2
, 
3

b: finger 
2

C: finger 
3

D: finger 
1
-
4
, 
7
-
9

E: finger 
1
-
4
, 
7
, 
8

F: finger 
1
-
4
, 
7

G: finger 
1
-
4

A: finger 
1
-
3

B: finger 
1
-
2

(Note that every finger is controlling a specific key, different fingers are controlling different keys.)

A finger presses a key if it is needed in a note, and if that same finger is not used in the note previously played. Naturally, every key needed for the first note is pressed because there is no previous note. Write a program to count the number of times each finger presses one of the keys.

Input and Output Specification
Each line in the input stream contains one song. The only characters allowed in the song are “cdefgabCDEFGAB”. There are at most 
200
 notes in a song, and the song may be empty.

For each test case, print 
10
 numbers indicating the number of presses for each finger. Numbers are separated by a single space.

Sample Input
cdefgab
BAGFEDC
CbCaDCbCbCCbCbabCCbCbabae
Sample Output
0 1 1 1 0 0 1 1 1 1
1 1 1 1 0 0 1 1 1 0
1 8 10 2 0 0 2 2 1 0
Resources
Java Set Operations
Example
Java HashSet
Turning it in
By the due date, turn in your java source code for the program using the submission server. The beginning of your source file must include a header like this:

/*
 * Author: name, e-mail address
 * Course: CSE 1002, Section 7, Spring 2022
 * Project: Eb Alto Saxophone
*/
The file name to submit must be Saxophone.java and the project id is saxophone. For your convenience, here is a submission form for this assignment.