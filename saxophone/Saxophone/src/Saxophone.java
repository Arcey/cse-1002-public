/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Eb Alto Saxophone
*/

import java.util.Scanner;
import java.util.Set;
import java.util.HashSet;

public final class Saxophone {

   static final int NUM_FINGERS = 10;

   // Fingers associated with a note
   static final Set<Integer> C_LOW = Set.of(2, 3, 4, 7, 8, 9, 10);
   static final Set<Integer> C_HIGH = Set.of(3);

   static final Set<Integer> D_LOW = Set.of(2, 3, 4, 7, 8, 9);
   static final Set<Integer> D_HIGH = Set.of(1, 2, 3, 4, 7, 8, 9);

   static final Set<Integer> E_LOW = Set.of(2, 3, 4, 7, 8);
   static final Set<Integer> E_HIGH = Set.of(1, 2, 3, 4, 7, 8);

   static final Set<Integer> F_LOW = Set.of(2, 3, 4, 7);
   static final Set<Integer> F_HIGH = Set.of(1, 2, 3, 4, 7);

   static final Set<Integer> G_LOW = Set.of(2, 3, 4);
   static final Set<Integer> G_HIGH = Set.of(1, 2, 3, 4);

   static final Set<Integer> A_LOW = Set.of(2, 3);
   static final Set<Integer> A_HIGH = Set.of(1, 2, 3);

   static final Set<Integer> B_LOW = Set.of(2);
   static final Set<Integer> B_HIGH = Set.of(1, 2);

   public static void main (final String[] args) {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");

      while (stdin.hasNext()) {
         display(countMovements(stdin.next()));
      }

      stdin.close();
   }

   // Returns finger movements needed to perform a melody
   private static int[] countMovements (final String melody) {
      final int[] fingerStrokes = new int[NUM_FINGERS];

      // Save movement of first note
      Set<Integer> previousNote = charToFingers(melody.charAt(0));
      addStrokes(fingerStrokes, previousNote);

      // Start at 1 because we need already checked against the first note
      for (int i = 1; i < melody.length(); i++) {
         final Set<Integer> thisNote = charToFingers(melody.charAt(i));

         // Symm Diff of the 2 sets gives every finger that needs to
         // move/isn't already in position
         final Set<Integer> newPresses = symmetricDifference(previousNote, thisNote);
         addStrokes(fingerStrokes, newPresses);

         // Intersection of both notes + thisNote returns only newly pressed and already
         // Pressed fingers
         previousNote = union(intersection(previousNote, thisNote), thisNote);
      }

      return fingerStrokes;
   }

   // Prints a given array of numbers to the output stream in form "1 2 3..."
   private static void display (final int[] numbers) {
      for (final int n : numbers) {
         System.out.printf("%d ", n);
      }
      System.out.printf("%n");
   }

   // Takes a list of fingers and set of strokes, and incremenets each finger's stroke
   private static void addStrokes (final int[] fingerStrokes,
         final Set<Integer> fingers) {
      for (final int n : fingers) {
         fingerStrokes[n - 1]++;
      }
   }

   // Takes a given character and returns set of fingers that must be pressed for note
   // Returns empty set if invalid note
   private static Set<Integer> charToFingers (final char c) {
      switch (c) {
      case 'c':
         return C_LOW;
      case 'C':
         return C_HIGH;
      case 'd':
         return D_LOW;
      case 'D':
         return D_HIGH;
      case 'e':
         return E_LOW;
      case 'E':
         return E_HIGH;
      case 'f':
         return F_LOW;
      case 'F':
         return F_HIGH;
      case 'g':
         return G_LOW;
      case 'G':
         return G_HIGH;
      case 'a':
         return A_LOW;
      case 'A':
         return A_HIGH;
      case 'b':
         return B_LOW;
      case 'B':
         return B_HIGH;
      default:
         return new HashSet<Integer>();
      }
   }

   private static Set<Integer> union (final Set<Integer> a, final Set<Integer> b) {
      final Set<Integer> c = new HashSet<>(a);
      c.addAll(b);
      return Set.copyOf(c);
   }

   private static Set<Integer> intersection (final Set<Integer> a, final Set<Integer> b) {
      final Set<Integer> c = new HashSet<>(a);
      c.retainAll(b);
      return Set.copyOf(c);
   }

   private static Set<Integer> symmetricDifference (final Set<Integer> a,
         final Set<Integer> b) {
      final Set<Integer> c = new HashSet<>(union(a, b));
      c.removeAll(union(a, intersection(a, b)));
      return Set.copyOf(c);
   }
}
