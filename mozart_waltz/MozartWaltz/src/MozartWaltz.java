/*
 * Author: Vincent Quintero, 
 * Course: CSE 1002, Section 7, Spring 2022
 * Project: Mozart Waltz
*/

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

// ! Minuet was generated using 11 sided die in sample, and is done the same here
// ! Despite the task asking for two 6-sided dice to be rolled for it. Idek
public final class MozartWaltz {

   static final int[][] MINUET_LUT = {
      { 96, 22, 141, 41, 105, 122, 11, 30, 70, 121, 26, 9, 112, 49, 109, 14 }, // 2
      { 32, 6, 128, 63, 146, 46, 134, 81, 117, 39, 126, 56, 174, 18, 116, 83 }, // 3
      { 69, 95, 158, 13, 153, 55, 110, 24, 66, 139, 15, 132, 73, 58, 145, 79 }, // 4
      { 40, 17, 113, 85, 161, 2, 159, 100, 90, 176, 7, 34, 67, 160, 52, 170 }, // 5
      { 148, 74, 163, 45, 80, 97, 36, 107, 25, 143, 64, 125, 76, 136, 1, 93 }, // 6
      { 104, 157, 27, 167, 154, 68, 118, 91, 138, 71, 150, 29, 101, 162, 23, 151 }, // 7
      { 152, 60, 171, 53, 99, 133, 21, 127, 16, 155, 57, 175, 43, 168, 89, 172 }, // 8
      { 119, 84, 114, 50, 140, 86, 169, 94, 120, 88, 48, 166, 51, 115, 72, 111 }, // 9
      { 98, 142, 42, 156, 75, 129, 62, 123, 65, 77, 19, 82, 137, 38, 149, 8 }, // 10
      { 3, 87, 165, 61, 135, 47, 147, 33, 102, 4, 31, 164, 144, 59, 173, 78 }, // 11
      { 54, 130, 10, 103, 28, 37, 106, 5, 35, 20, 108, 92, 12, 124, 44, 131 } // 12
   };

   static final int[][] TRIO_LUT = {
         { 72, 6, 59, 25, 81, 41, 89, 13, 36, 5, 46, 79, 30, 95, 19, 66 }, // 1
         { 56, 82, 42, 74, 14, 7, 26, 71, 76, 20, 64, 84, 8, 35, 47, 88 }, // 2
         { 75, 39, 54, 1, 65, 43, 15, 80, 9, 34, 93, 48, 69, 58, 90, 21 }, // 3
         { 40, 73, 16, 68, 29, 55, 2, 61, 22, 67, 49, 77, 57, 87, 33, 10 }, // 4
         { 83, 3, 28, 53, 37, 17, 44, 70, 63, 85, 32, 96, 12, 23, 50, 91 }, // 5
         { 18, 45, 62, 38, 4, 27, 52, 94, 11, 92, 24, 86, 51, 60, 78, 31 } // 6
   };

   static final Random RNG = new Random(Long.getLong("seed", System.nanoTime()));
   static final String AUDIO_SRC = 
         "http://andrew.cs.fit.edu/~cse1002-stansifer/mozart/wav/";
   static final String SAVE_FILENAME = "waltz.wav";
   static final String CMD_PLAY = "play";
   static final int MEASURES = 16;
   static final int TRIO_DIE = 6;
   static final int MINUET_DIE = 11;  // Sample output created using AN 11 SIDED DIE??
   static final int WALTZ_PARTS = 2;

   // Thrown because URL requires an exception to be handled, but we want to crash on err
   public static void main (final String[] args) throws IOException {
      // debugRandom();
      final int[] minuet = composeMinuet();
      final int[] trio = composeTrio();
      
      displayArray("Minuet:", minuet);
      displayArray("Trio:", trio);
      final double[] waltz = composeWaltz(minuet, trio);
      System.out.printf("Music Size: %d%n", waltz.length);

      if (args[0].equals(CMD_PLAY)) {
         MyAudio.play(waltz);
      } else { 
         MyAudio.save(SAVE_FILENAME, waltz);
      }
   }

   // Prints leading text followed by each element of the of given arr (preceded by
   // whitespace), and concluded with a single newline
   private static void displayArray (final String leading, final int[] arr) {
      System.out.printf("%s", leading);
      for (final int i : arr) {
         System.out.printf(" %d", i);
      }
      System.out.printf("%n");
   }

   private static int[] composeMinuet () {
      final int[] minuet = new int[MEASURES];

      for (int i = 0; i < minuet.length; i++) {
         minuet[i] = MINUET_LUT[RNG.nextInt(MINUET_DIE)][i];
      }

      return minuet;
   }

   private static int[] composeTrio () {
      final int[] trio = new int[MEASURES];

      for (int i = 0; i < trio.length; i++) {
         trio[i] = TRIO_LUT[RNG.nextInt(TRIO_DIE)][i];
      }

      return trio;
   }

   private static double[] composeWaltz (final int[] minuet, final int[] trio) 
         throws IOException  {
      final ArrayList<Double> waltz = new ArrayList<>();

      for (int i = 0; i < minuet.length; i++) {
         final double[] audio = MyAudio.read(new URL(AUDIO_SRC + "M"
                + Integer.toString(minuet[i]) + ".wav"));
         for (final double d : audio) { // Flatten out the received array of doubles
            waltz.add(d);
         }
      }

      for (int i = 0; i < trio.length; i++) {
         final double[] audio = MyAudio.read(new URL(AUDIO_SRC + "T" 
               + Integer.toString(trio[i]) + ".wav"));
         for (final double d : audio) {
            waltz.add(d);
         }
      }

      // For each double in the arraylist, convert it from Double to double This is
      // needed because ArrayList<Double> cannot directly convert to double array
      return waltz.stream().mapToDouble(n -> n).toArray();
   }
}
