/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Ticket
*/

import java.util.Arrays;
import java.util.Scanner;

public final class Ticket {
    static int NUMBER = 1;

    private record Entry (int row, int col, double distance) implements Comparable<Entry> {
        @Override
        public int compareTo (final Entry other) {
            if (this.distance < other.distance) {
                return -1;
            } else if (this.distance > other.distance) {
                return 1;
            }
            return 0;
        }
    };

    public static void main (final String[] args) {
        final Scanner stdin = new Scanner(System.in, "US-ASCII");
        final int cols = stdin.nextInt();
        final int rows = stdin.nextInt();

        Entry[][] table = new Entry[rows][cols];

        /**
         * pop 2d array with distances
         * sort 2d array by distances, then rows, then cols
         * compare with left priority
         */

        // Add 1 because zero offset
        int nums = 1;
        final int refRow = table.length - 1;
        final int refCol = table[0].length / 2 - 1;
        final double refPoint = Math.hypot(table[0].length / 2 + 1, table.length);
        for (int i = 0; i <= refRow; i++) {
            for (int j = 0; j <= refCol; j++) {
                table[i][j] = new Entry(i, j, nums);
                nums += 1;
            }
        }

        System.out.printf("%s%n", Arrays.toString(table[0]));

        for (int i = 0; i < refRow; i++) {
            for (int j = refCol; j < cols; j++) {
                table[i][j] = new Entry(i, j, nums);
                nums++;
            }
        }



        if (table[3][8].distance < table[4][1].distance) {
            System.out.println("TRUE");
        } else {
            System.out.println("FALSE");
        }

        showTable(table);

        stdin.close();
    }

    private static void showTable (Entry[][] table) {
        System.out.printf("        1  2  3  4  5  6  7  8  9  10  11%n");
        System.out.printf("      +----------------------------------%n");
        int j = 1;
        for (final Entry[] t : table) {
            System.out.printf("Row %d |", j);
            for (final Entry i : t) {
                System.out.printf(" %f", i.distance);
            }
            System.out.printf("%n");
            j++;
        }
    }

    public static Entry[][] sortColumnsProperly(Entry[][] m) {
        // Sort each colum.
        for (int col = 0; col < m[0].length; col++) {
            // Pull the column out.
            Entry[] thisCol = new Entry[m.length];
            for (int i = 0; i < m.length; i++) {
                thisCol[i] = m[i][col];
            }
            // Sort it.
            Arrays.sort(thisCol);
            // Push it back in.
            for (int i = 0; i < m.length; i++) {
                m[i][col] = thisCol[i];
            }
        }
        return m;
    
    }
}
