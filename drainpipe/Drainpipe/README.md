CSE 1002: Wye Drainpipe
Due: 20 April 2022, 8pm

Additional practice is always available at CodingBat Java, Project Euler, and Kattis.

Task
Hugh Manatee directs the starving manatees in a single line down the Indian River Lagoon (IRL) toward the south in hopes of finding more seagrass. The manatees are already in a single line, but not in the right order. Hugh wants to rearrange the manatees in order of priority so that the most needy manatees arrive first. However, the low water-level in the narrow channels requires some cleverness to maneuver the large water mamals into the desired position. Hugh has found space in front of a long drainpipe in which is is possible to turn a manatee and back it into the drainpipe. By backing each manatee into the drainpipe and moving the manatees out of the drainpipe at just the right moment, Hugh hopes to get them all in the right order to continue the journey in single file.

Given the desired order in which the manatees are to arrive down south, your task is to dermine if there is any way Hugh can use the drainpipe to achive the desired order.

Input
All input is to be read from the standard input stream.

The input consists of several blocks of lines and is terminated by a line with just 0.

The first line of each block indicates the number of manatees 0 < N < 100,000. In each of the next lines of the block there is a permutation of the numbers 1, 2, ..., N. For N=3, the permutation 2 3 1 means that the first manatee needs to be placed at the end of line and that the second manatee needs to be placed at the front of the line. The last line of each block contains just 0 signaling the end of the block.

Output
For each permutation in the input, the output is either the word "Yes" or "No" on a line by itself. See the sample below.

Sample Input
5           ;; N=5 manatees  1 2 3 4 5
1 2 3 4 5
5 4 1 2 3
0           ;; End of first test case
6           ;; N=6 manatees  1 2 3 4 5 6
6 5 4 3 2 1
0           ;; End of second test case
0           ;; No more test cases
(The text in blue is commentary and will not be present in the input.)
Sample Output
Yes     ;; 1 2 3 4 5 <-- 1 2 3 4 5 is possible
No      ;; 5 4 1 2 3 <-- 1 2 3 4 5 is NOT possible
Yes     ;; 6 5 4 3 2 1 <-- 1 2 3 4 5 6 is possible
(The text in blue is commentary and must not appear in the output.)