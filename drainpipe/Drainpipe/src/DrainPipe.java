/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Wye Drainpipe
*/

import java.util.Scanner;
import java.util.ArrayDeque;

public final class DrainPipe {
   public static void main (final String[] args) {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");

      while (stdin.hasNext()) {

         final int numManatees = stdin.nextInt();

         if (numManatees != 0) {
            createBlock(stdin, numManatees);
         } else {
            break; // End program
         }
      }

      stdin.close();
   }

   // Creates a "safe area" where permutations can consistently be read and deemed viable
   private static void createBlock (final Scanner stdin, final int manatees) {
      // Guaranteed at least one safe permutation read, otherwise this function wouldn't
      // Have been invoked
      int initial = stdin.nextInt(); // If nonzero, first element of permutation
      while (initial != 0) {
         final int[] permutation = new int[manatees];
         permutation[0] = initial;

         for (int i = 1; i < manatees; i++) {
            permutation[i] = stdin.nextInt();
         }

         if (viableReorder(permutation)) {
            System.out.printf("Yes%n");
         } else {
            System.out.printf("No%n");
         }

         initial = stdin.nextInt(); // Recheck to determine if rerun is necessary
      }
   }

   // Determine if reordering manatees in desired order is possible
   private static boolean viableReorder (final int[] desiredOrder) {
      final int[] manatees = initManatees(desiredOrder.length);
      final ArrayDeque<Integer> pipe = new ArrayDeque<Integer>();

      // The last element in the stack is the highest value manatee thus far.
      // If desired Manatee > pipe.peek(), then the manatee must still be in the array
      // ! This completely fails if the array of manatees (not desiredOrder) is unsorted
      int huntingIndex = 0;
      for (int i = 0; i < desiredOrder.length; i++) {
         pipe.push(manatees[i]);

         // The manatee we want is the next manatee in the pipe
         if (pipe.peek() == desiredOrder[huntingIndex]) {
            pipe.pop(); 
            huntingIndex++; 

            // Check if the new next manatee can now be added
            huntingIndex = tryNextManatee(pipe, desiredOrder, huntingIndex);

         }
         if (!pipe.isEmpty() && pipe.peek() > desiredOrder[huntingIndex]) {
            // Desired manatee has lower priority than stack's head + remaining manatees,
            // thus it is stuck in back of pipe
            return false;
         }
      }
      return true;
   }

   // Recursively check if manatee in the pipe is the next desired one
   // Repeat until the above is false, return index which made it false
   private static int tryNextManatee (final ArrayDeque<Integer> pipe, 
         final int[] desiredOrder, final int index) {

      if (pipe.isEmpty() || pipe.peek() != desiredOrder[index]) {
         return index;
      }

      pipe.pop();
      return tryNextManatee(pipe, desiredOrder, index + 1);
   }

   // Return array of N manatees in form [1,2,3,... N]
   private static int[] initManatees (final int numManatees) {
      final int[] manatees = new int[numManatees];

      for (int i = 0; i < manatees.length; i++) {
         manatees[i] = i + 1;
      }

      return manatees;
   }
}
