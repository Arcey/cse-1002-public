/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Josephus Problem
*/

import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

public final class Josephus {
   static final double NANOS_PER_SECOND = 1_000_000_000.0; // Total nanos in one second
   static final int DESIRED_SURVIVORS = 1;
   static final int TRIALS = 10;

   public static void main (final String[] args) throws Exception {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");
      final int stepSize = Integer.parseInt(args[0]);
      final Class<?> clazz = Class.forName(args[1].toString());
      @SuppressWarnings("unchecked")
      // These names will never be altered
      final java.util.List<String> safeNames = (java.util.List<String>) 
            clazz.getDeclaredConstructor().newInstance();
      // These names will be altered every trial, easier to scope outside loop
      final java.util.List<String> names = (java.util.List<String>) 
            clazz.getDeclaredConstructor().newInstance();

      while (stdin.hasNext()) {
         safeNames.add(stdin.next());
      }

      long total = 0;
      for (int i = 1; i <= TRIALS; i++) {
         // Reset names to original test value
         names.clear();
         deepCopy(safeNames, names);

         final long start = System.nanoTime();
         simulate(names, stepSize - 1);
         final long stop = System.nanoTime();

         total += stop - start;
      }
      System.out.printf("Last soldier: %s.  Time: PT%.9fS%n", names.get(0),
            nanoToSeconds(total / TRIALS));

      stdin.close();
   }

   // Copies ONTO existing destination (If values preexist, they will not be modified)
   private static void deepCopy (final List<String> src, final List<String> dest) {
      for (final String val : src) {
         dest.add(val);
      }
   }

   private static double nanoToSeconds (final long nanoTime) {
      return nanoTime / NANOS_PER_SECOND;
   }

   // Returns name of surviving soldier
   private static void simulate (final List<String> names, final int skip) {
      ListIterator<String> namesIterator = names.listIterator();

      int numSkipped = 0;
      while (names.size() != DESIRED_SURVIVORS) {
         if (namesIterator.hasNext()) {
            // Must call next to iterate to next name & let remove() function
            namesIterator.next();

            if (numSkipped == skip) {
               namesIterator.remove();
               numSkipped = 0;
            } else {
               numSkipped++;
            }
         } else {
            // Reset the list iterator
            namesIterator = names.listIterator();
         }
      }
   }
}
