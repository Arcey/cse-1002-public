/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Search a List
*/

import java.text.Collator;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public final class Search {

   static final String QUERY_PREFIX = "?"; // Symbol denoting query string
   static final int QUERY_MIN = 2; // Minimum elements required to perform query

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner(System.in, "US-ASCII");
      ArrayList<String> manatees = new ArrayList<String>();

      while (scanner.hasNext()) {
         final String input = scanner.next();

         if (input.compareTo(QUERY_PREFIX) == 0) {
            // Queries are always followed by two names, do 2 scans to get them
            manatees = handleQuery(manatees, scanner.next(), scanner.next());
         } else {
            manatees.add(input);
         }
      }

      scanner.close();
   }

   private static ArrayList<String> handleQuery (final ArrayList<String> manatees,
         final String man1, final String man2) {
      final int start = manatees.indexOf(man1);
      final int stop = manatees.indexOf(man2);

      // Both manatees exist in the list
      if (start != -1 && stop != -1) {

         // The queried manatees are ordered properly
         if (start <= stop) {
            // subList is exclusive so add 1 for desired value
            final ArrayList<String> newManatees = new
                   ArrayList<String>(manatees.subList(start, stop + 1));
                   
            listManatees(newManatees);
            return newManatees;
         }
         System.out.printf("Empty List%n");
      } else {
         System.out.printf("Not found%n");
      }

      return manatees;
   }

   // Prints the list of given manatees to the screen, sorted alphabetically
   private static void listManatees (final ArrayList<String> manatees) {
      // Java doesn't pass deep copies, so we need a new list to avoid ruining
      // manatees
      final ArrayList<String> sortedManatees = new ArrayList<String>(manatees);
      sortedManatees.sort(Collator.getInstance(Locale.US));

      for (int i = 0; i < sortedManatees.size(); i++) {
         System.out.printf("%3d: %s%n", i + 1, sortedManatees.get(i));
      }
   }
}
