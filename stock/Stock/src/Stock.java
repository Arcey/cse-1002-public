/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Project: Stock
*/

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Stock {

   static final String DELIMITER = ",";
   static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter
         .ofPattern("E, dd LLL YYYY").withLocale(Locale.US);

   public static void main (final String[] args) {
      final Scanner scanner = new Scanner(System.in, "US-ASCII").useDelimiter(DELIMITER);
      final ArrayList<LocalDate> dates = new ArrayList<LocalDate>();
      final ArrayList<Double> closings = new ArrayList<Double>();

      // Skip headers line
      scanner.nextLine();

      while (scanner.hasNext()) {
         dates.add(LocalDate.parse(scanner.next())); // Get date
         scanner.next(); // Ignore open
         scanner.next(); // Ignore high
         scanner.next(); // Ignore low
         scanner.next(); // Ignore close
         closings.add(scanner.nextDouble()); // Get adj closing price
         scanner.nextLine(); // Ignore anything leftover
      }

      findMaxGainLoss(dates, closings);

      scanner.close();
   }

   // Calculate the max gain & loss, the longest streak of a gain, then shows them
   private static void findMaxGainLoss (final ArrayList<LocalDate> dates,
         final ArrayList<Double> closings) {
      int maxGainIndex = 1;
      int maxLossIndex = 1;
      double maxGain = 0.0;
      double maxLoss = Double.MAX_VALUE;
      int streakStart = 0;
      int streakStop = 0;
      boolean growing = false;

      for (int i = closings.size() - 1; i > 0; i--) {

         // Current day's close is a gain greater than the previous best
         if (closings.get(i) > closings.get(i - 1)) {
            // This is the best closing so far
            if (closings.get(i) > maxGain) {
               maxGain = closings.get(i);
               maxGainIndex = i;

               if (!growing) {
                  streakStop = i;
               }
               growing = true;
            }

         } else if (closings.get(i) < closings.get(i - 1)) {
            if (closings.get(i) < maxLoss) {
               maxLoss = closings.get(i);
               maxLossIndex = i;

               if (growing) {
                  streakStart = i;
               }
               growing = false;
            }
         }
      }

      // Since maxIndexes can never be 0, they will never be OOB
      showGainLoss(
            percentOf(closings.get(maxGainIndex - 1), closings.get(maxGainIndex)),
            dates.get(maxGainIndex),
            percentOf(closings.get(maxLossIndex - 1), closings.get(maxLossIndex)),
            dates.get(maxLossIndex),
            streakStop - streakStart,
            dates.get(streakStart + 1), // Offset by 1 because of exclusive for 
            dates.get(streakStop));
   }

   // Handles displaying the gain, loss, and streak to the output stream
   private static void showGainLoss (final double gainPercent, final LocalDate gainDate,
         final double lossPercent, final LocalDate lossDate, final int streak,
         final LocalDate streakStart, final LocalDate streakStop) {
      System.out.printf("Max gain: %.2f%% on %s%n", gainPercent,
            gainDate.format(TIME_FORMAT));
      System.out.printf("Max loss: %.2f%% on %s%n", lossPercent,
            lossDate.format(TIME_FORMAT));
      System.out.printf("Longest growth streak: %d days (%s to %s)%n",
            streak, streakStart.format(TIME_FORMAT),
            streakStop.format(TIME_FORMAT));
   }

   // Calculate what percent of a the value b would be over/under by
   private static double percentOf (final double a, final double b) {
      return((100 / a) * b - 100);
   }
}
