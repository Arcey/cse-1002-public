/*
* Author:    Vincent Quintero
* Course:    CSE 1002, Spring 2022
* Project:   Proj 04, Table
*/

public final class Table {
   public static void main (final String[] args) {
      // args[] is an array of strings. If we want to use the value,
      // we have to convert it from string to int
      final int n = Integer.parseInt (args[0]);

      // Print header
      System.out.printf ("%7s %-6s %9s %9s %9s %8s%n",
            "i", "hex", "i*i", "i*i*i", "log", "pcnt");

      for (int i = 1; i <= n; i++) {
         final int I_SQUARE = i * i;

         printRow (i, I_SQUARE, I_SQUARE * i, logBase2 (I_SQUARE),
               percentComplete (n, i));
      }
   }

   public static void printRow (final int n, final int nSquared, final int nCubed,
         final double nLog, final int nPerc) {
      System.out.printf ("%7d 0x%04X %,9d %,9d %9.3f %8d%%%n",
            n, n, nSquared, nCubed, nLog, nPerc);
   }

   // Return logarithm base 2 of value n
   public static double logBase2 (final double n) {
      return Math.log (n) / Math.log (2);
   }

   // Calculate current (integer) percentage complete out of some whole
   public static int percentComplete (final int max, final int current) {
      return 100 / max * current;
   }
}
