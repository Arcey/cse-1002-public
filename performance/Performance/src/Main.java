/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Selection Sort
*/

import java.util.Collections;
import java.util.List;
import java.util.Random;

public final class Main {
   static final Random RNG = new Random(Long.getLong("seed", System.nanoTime()));
   static final double NANOS_PER_SECOND = 1_000_000_000.0; // Total nanos in one second
   static final int TRIALS = 10;

   public static void main (final String[] args) throws Exception {
      final int listSize = Integer.parseInt(args[0]);
      final Class<?> clazz = Class.forName(args[1].toString());
      @SuppressWarnings("unchecked")
      final java.util.List<Integer> list = 
            (java.util.List<Integer>) clazz.getDeclaredConstructor().newInstance();

      initList(list, listSize);

      long total = 0;
      for (int i = 0; i < TRIALS; i++) {
         Collections.shuffle(list, RNG);
         final long startTime = System.nanoTime();
         sort(list);
         final long endTime = System.nanoTime();
         total += endTime - startTime;
      }
      System.out.printf("PT%fS%n", nanoToSeconds(total / TRIALS));
   }

   public static double nanoToSeconds (final long nanoTime) {
      return nanoTime / NANOS_PER_SECOND;
   }

   public static void initList (final List<Integer> data, final int size) {
      for (int i = 1; i <= size; i++) {
         data.add(i);
      }
   }

   // Selection sort
   public static void sort (final List<Integer> data) {
      for (int i = 0; i < data.size(); i++) {
         int minIndex = i;

         for (int j = i + 1; j < data.size(); j++) {
            // Is new element smaller than current min
            if (data.get(j) < data.get(minIndex)) {
               minIndex = j;
            }
         }
         /* swap data at 'min' with data at 'i' */
         final int temp = data.get(i);
         data.set(i, data.get(minIndex));
         data.set(minIndex, temp);
      }
   }
}
