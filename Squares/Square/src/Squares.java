/*
* Author:    Vincent Quintero, 
* Course:    CSE 1002, Spring 2022
* Project:   Fun with Squares
*/

public final class Squares {

   static final double ORIGIN_X = 0.5;
   static final double ORIGIN_Y = 0.5;
   static final double INITIAL_SIDE_L = 0.5;

   public static void main (final String[] args) {
      final double ratio = Double.parseDouble(args[0]);
      final int level = Integer.parseInt(args[1]);
      final int pattern = Integer.parseInt(args[2]);

      switch (pattern) {
      case 1:
         pattern1(ORIGIN_X, ORIGIN_Y, INITIAL_SIDE_L, ratio, 0, level);
         break;
      case 2:
         pattern2(ORIGIN_X, ORIGIN_Y, INITIAL_SIDE_L, ratio, 0, level);
         break;
      case 3:
         pattern3(ORIGIN_X, ORIGIN_Y, INITIAL_SIDE_L, ratio, 0, level);
         break;
      case 4:
         pattern4(ORIGIN_X, ORIGIN_Y, INITIAL_SIDE_L, ratio, 0, level);
         break;
      default:
         pattern1(ORIGIN_X, ORIGIN_Y, INITIAL_SIDE_L, ratio, 0, level);
         break;
      }
   }

   private static void pattern1 (final double x, final double y, final double sideLength,
         final double ratio, final int n, final int level) {
      if (n >= level) {
         return;
      }

      /*
       * Half of the original side's length {sideLength} is the full side length of
       * the new square. Half of the new square's side length would give the offset
       * needed to place the square's origin on a corner. Thus we divide by 4, not 2
       */
      final double sideLengthOffset = sideLength / 4;
      final double sideRatio = sideLength / ratio;

      // Divide {sideRatio} by 2 in x/y coords because {sideRatio} is full length side
      pattern1(x + sideLengthOffset + sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top right
      pattern1(x + sideLengthOffset + sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom right
      pattern1(x - sideLengthOffset - sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom left
      pattern1(x - sideLengthOffset - sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top left

      // When drawing, half of the square's full side is expected so we mult by 2
      drawSquare(x, y, 2 * sideLengthOffset);
   }

   private static void pattern2 (final double x, final double y, final double sideLength, 
         final double ratio, final int n, final int level) {
      if (n >= level) {
         return;
      }

      final double sideLengthOffset = sideLength / 4;
      final double sideRatio = sideLength / ratio;

      pattern2(x + sideLengthOffset + sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top right
      pattern2(x - sideLengthOffset - sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom left
      pattern2(x - sideLengthOffset - sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top left
            
      drawSquare(x, y, 2 * sideLengthOffset);

      pattern2(x + sideLengthOffset + sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom right

   }

   private static void pattern3 (final double x, final double y, final double sideLength, 
         final double ratio, final int n, final int level) {
      if (n >= level) {
         return;
      }

      final double sideLengthOffset = sideLength / 4;
      final double sideRatio = sideLength / ratio;

      drawSquare(x, y, 2 * sideLengthOffset);

      pattern3(x + sideLengthOffset + sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top right
      pattern3(x - sideLengthOffset - sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom left
      pattern3(x - sideLengthOffset - sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top left
      pattern3(x + sideLengthOffset + sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom right
   }

   private static void pattern4 (final double x, final double y, final double sideLength, 
         final double ratio, final int n, final int level) {
      if (n >= level) {
         return;
      }

      final double sideLengthOffset = sideLength / 4;
      final double sideRatio = sideLength / ratio;

      pattern4(x + sideLengthOffset + sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top right
      pattern4(x - sideLengthOffset - sideRatio / 2, y + sideLengthOffset + sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Top left
            
      drawSquare(x, y, 2 * sideLengthOffset);

      pattern4(x - sideLengthOffset - sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom left
      pattern4(x + sideLengthOffset + sideRatio / 2, y - sideLengthOffset - sideRatio / 2,
            sideRatio, ratio, n + 1, level); // Bottom right
   }

   private static void drawSquare (final double x, final double y, final double side) {
      StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
      StdDraw.filledSquare(x, y, side);
      StdDraw.setPenColor(StdDraw.BLACK);
      StdDraw.square(x, y, side);
   }
}
