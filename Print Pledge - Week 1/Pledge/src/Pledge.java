/*
* Author:    Vincent Quintero
* Course:    CSE 1002, Spring 2022
* Project:   Proj 02, Print Pledge
*/

public final class Pledge {
   static final String[] PLEDGE = {
         "I pledge that every program with my name on it shall be written by me\n",
         "(and my co-authors, if any).  Every program I submit shall be entirely\n",
         "my own work unless otherwise attributed.  I understand that academic\n",
         "dishonesty not only includes copying other people's work,  but also\n",
         "abetting or facilitating copying.  Code that is substantially similar\n",
         "to any other submission past or present will get no credit whatever\n",
         "the explanation.  I understand that the consequence of academic\n",
         "dishonesty is a grade of 'F' for the class. I pledge to devote my\n",
         "efforts to learning and exploring Java by writing and testing my own\n",
         "programs.  I shall strive to be attentive to detail and write programs\n",
         "that not only compile and execute correctly, but are easily\n",
         "understandable by myself and other programmers.\n", };

   public static void main (final String[] args) {
      for (final String line : PLEDGE) {
         System.out.print(line);
      }
   }
}
