Background
Task
Some buildings with electronic locks keep a record of when a person enters and leaves the building. The fire marshall is interested in ensuring that the maximum occupence of the building is never exceeded. Your task is to read in the log and report to the first marshall how many people were in the building. There are N entries in the building log. And the fire marshall makes M queries.

Input
The first line of the input contains two integers N>0 and M>0, the number of entries in the log and the number of queries respectively. The next N lines are log entries each on a line. The line contains three, white-space separated tokens: a string name, an integer 0<T<100,000, and a duration 0<D<100,000. The next M lines are queries each on a line. The line contains two, white-space integers: a time 0<T<100,000 and a duration 0<D<100,000.

Output
For each query the output is the number of people who were in the building at least for part of the interval.

Sample Input and output
2 1
X 234 567
Y 456   1
460 5
Output
1    # only 'X' was in the builing some time in interval 460..465
Hints:
"Ryan 234 567" means Ryan entered the building at 234 minutes and stayed for 567 minutes
Try to make a database of records
Because of the constraints ( 1 ≤ N < 10 000 and 1 ≤ M < 100), the brute force approach is fine. Therefore a trivial check for all the people for each interval works.
A better solution would be sorting the records of ending and starting times of all people and using binay search. This is a much faster solution.