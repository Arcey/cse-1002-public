/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Fire Marshall Count
*/

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public final class Fire {
   private record LogEntry (String name, int time, int duration) {
      public int stay () {
         return this.time + this.duration;
      }
   };

   private record Query (int time, int duration) {
      public int stay () {
         return this.time + this.duration;
      }
   };

   public static void main (final String[] args) {
      final Scanner stdin = new Scanner(System.in, "US-ASCII");
      final int numLogs = stdin.nextInt();
      final int numQueries = stdin.nextInt();
      stdin.nextLine(); // Terminate line that held above values

      final LogEntry[] logs = logsFromInput(stdin, numLogs);
      final Query[] queries = queriesFromInput(stdin, numQueries);

      // Sort log entries from shortest to longest time in the building
      Arrays.sort(logs, Comparator.comparing((final LogEntry log) -> log.stay()));

      for (final Query q : queries) {
         int people = 0;
         for (final LogEntry entry : logs) {
            if (inBuilding(entry, q)) {
               people++;
            }
         }
         System.out.printf("%d%n", people);
      }

      stdin.close();
   }

   // Determines if a person (a) is inside the building at any point in query (b)
   private static boolean inBuilding (final LogEntry a, final Query b) {
      // Person enters building at or before the end of the query &
      // Remains in the building long enough to be counted in the query
      if (a.time <= b.stay() && a.stay() >= b.time) {
         return true;
      }
      return false;
   }

   private static Query[] queriesFromInput (final Scanner input, final int numQueries) {
      final Query[] queries = new Query[numQueries];

      for (int i = 0; i < numQueries; i++) {
         queries[i] = new Query(input.nextInt(), input.nextInt());
      }

      return queries;
   }

   private static LogEntry[] logsFromInput (final Scanner input, final int numLogs) {
      final LogEntry[] logs = new LogEntry[numLogs];

      for (int i = 0; i < numLogs; i++) {
         logs[i] = new LogEntry(input.next(), input.nextInt(), input.nextInt());
      }

      return logs;
   }
}
