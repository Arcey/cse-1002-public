/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

public final class Absence implements Living {
   @Override
   public Living next (final Neighborhood n) {
      // Creates Fox: Rabbit > 2
      if (n.getCount(Rabbit.class) > 2) {
         return new Fox();

         // Creates Rabbit: Grass > 4
      } else if (n.getCount(Grass.class) > 4) {
         return new Rabbit();

         // Creates Grass: Grass > 0
      } else if (n.getCount(Grass.class) > 0) {
         return new Grass();
      }
      return this;
   }

   @Override
   public char getCharRepresentation () {
      return '.';
   }
}
