/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

public final class Rabbit implements Living {
   private static final int MAX_AGE = 5;
   private int age = 0;

   @Override
   public Living next (final Neighborhood n) {
      // Rabbit dies: No grass in neighborhood or number of foxes >= number of rabbits in
      // neighborhood or old age
      if ((n.getCount(Grass.class) == 0) // Starves to death
            || (n.getCount(Fox.class) > 0  // Eaten (1): At least 1 Fox
            && n.getCount(Fox.class) >= n.getCount(Rabbit.class)) // Eaten(2): Fox >= Rab
            || this.age == Rabbit.MAX_AGE) { // Dies of old age
         return new Absence();
      }
      this.age++;
      return this;
   }

   @Override
   public char getCharRepresentation () {
      return 'R';
   }
}
