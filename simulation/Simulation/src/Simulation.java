/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

import java.util.Random;

public final class Simulation {

   static final Random RNG = new Random(Long.getLong("seed", System.nanoTime()));
   // % Chance to spawn an animal (exclusive) out of 1.00 
   static final double SPAWN_FOX = 0.1;
   static final double SPAWN_RABBIT = 0.3;
   static final double SPAWN_GRASS = 0.6;  

   public static void main (final String[] args) {
      final int size = Integer.parseInt(args[0]);
      final int cycles = Integer.parseInt(args[1]);
      World oldWorld = new World(genWorld(size));

      for (int i = 0; i <= cycles; i++) {
         System.out.printf("Cycle = %d%n", i);
         oldWorld.printWorld();
         oldWorld = oldWorld.cycle();
         System.out.printf("%n");
      }
   }

   private static Living[][] genWorld (final int size) {
      final Living[][] world = new Living[size][size];

      for (int i = 0; i < size; i++) {
         for (int j = 0; j < size; j++) {
            // Ensures that an animal isn't generated on the border
            if ((i != 0 && i != size - 1) && (j != 0 && j != size - 1)) {
               world[i][j] = genAnimal();
            } else {
               world[i][j] = new Absence();
            }
         }
      }

      return world;
   }

   private static Living genAnimal () {
      final double x = RNG.nextDouble();
      if (x < SPAWN_FOX) {
         return new Fox();
      } else if (x < SPAWN_RABBIT) {
         return new Rabbit();
      } else if (x < SPAWN_GRASS) {
         return new Grass();
      }
      return new Absence();
   }
}
