/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

public final class World {
   private final Living[][] world;

   public World (final Living[][] world) {
      this.world = world;
   }

   public void printWorld () {
      for (int i = 0; i < this.world.length; i++) {
         for (int j = 0; j < this.world.length; j++) {
            System.out.printf("%s", this.world[i][j].getCharRepresentation());
         }
         System.out.printf("%n");
      }
   }

   public Living at (final int x, final int y) {
      return this.world[x][y];
   }

   public void replaceAt (final int x, final int y, final Living replaceWith) {
      this.world[x][y] = replaceWith;
   }

   // Deeply copy this world into a new World instance and return it
   public World copy () {
      final Living[][] newWorld = new Living[this.world.length][this.world.length];

      for (int i = 0; i < this.world.length; i++) {
         for (int j = 0; j < this.world.length; j++) {
            newWorld[i][j] = this.world[i][j];
         }
      }

      return new World(newWorld);
   }

   // Returns a new World created by cycling the current (given) world
   public World cycle () {
      // We do not want to modify the existing world, so deep copy it
      final World newWorld = this.copy();
      for (int j = 1; j < this.world.length - 1; j++) {
         for (int k = 1; k < this.world.length - 1; k++) {
            final Living obj = this.at(j, k);
            // Next cycle's state is generated from previous cycle
            // Use this' Neighborhood to not ruin next cycle building off itself
            newWorld.replaceAt(j, k, obj.next(new Neighbors(j, k, this)));
         }
      }
      return newWorld;
   }
}
