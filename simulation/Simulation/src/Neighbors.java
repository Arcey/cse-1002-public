/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

public final class Neighbors implements Neighborhood {
   private final int x;
   private final int y;
   private final World world;

   public Neighbors (final int x, final int y, final World world) {
      this.x = x;
      this.y = y;
      this.world = world;
   }

   @Override
   public int getCount (final Class<?> c) {
      int neighbors = 0;

      for (int i = this.x - 1; i <= this.x + 1; i++) {
         for (int j = this.y - 1; j <= this.y + 1; j++) {
            // Ensure "this" is not counting itself as a neighbor
            if (this.x != i || this.y != j) {
               if (c == this.world.at(i, j).getClass()) {
                  neighbors++;
               }
            }
         }
      }

      return neighbors;
   }

}
