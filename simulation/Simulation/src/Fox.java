/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

public final class Fox implements Living {
   private static final int MAX_AGE = 5;
   private int age = 0;

   @Override
   public Living next (final Neighborhood n) {
      // Fox dies: More than 5 foxes neighboring or 0 neighboring rabbits or old age
      if (n.getCount(Fox.class) > 5
            || n.getCount(Rabbit.class) == 0
            || this.age == Fox.MAX_AGE) {
         return new Absence();
      }
      this.age++;
      return this;
   }

   @Override
   public char getCharRepresentation () {
      return 'X';
   }

}
