/*
* Author: Vincent Quintero, 
* Course: CSE 1002, Section 7, Spring 2022
* Project: Predator-Prey Simulation
*/

public final class Grass implements Living {
   @Override
   public Living next (final Neighborhood n) {
      // Grass replaced by rabbit: Grass > 2 * Rabbits in neighborhood
      if (n.getCount(Grass.class) > 2 * n.getCount(Rabbit.class)) {
         return new Rabbit();
         // Grass remains: Grass > Rabbits
      } else if (n.getCount(Grass.class) > n.getCount(Rabbit.class)) {
         return this;
      }
      return new Absence();
   }

   @Override
   public char getCharRepresentation () {
      return 'G';
   }
}
